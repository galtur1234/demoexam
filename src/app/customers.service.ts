import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  customers = [{name:'gal', years:20, income:20000}, {name:'yarin', years:20, income:20000}];  
  customerCollection:AngularFirestoreCollection;  
  
  public getCustomers(userId){
    this.customerCollection = this.db.collection(`users/${userId}/customers`);
    return this. customerCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }

  deleteCustomer(Userid:string, id:string){
    this.db.doc(`users/${Userid}/customers/${id}`).delete(); 
  } 


  updateCustomer(userId:string,id:string,name:string,years:number, income:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        income:income
      }
    )
  }

  save(userId:string,id:string,name:string,years:number,income:number,category:string,ST:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        incame:income,
        category:category,
        ST:ST
      }
    )
  }
  addCustomer(userId:string,name:string,years:number,income:number,category:string,ST:number){
    const customer = {name:name, years:years, income:income, category:category,ST:ST}; 
    this.userCollection.doc(userId).collection('customers').add(customer);
  }

constructor(private db:AngularFirestore) { }
}
