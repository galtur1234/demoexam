export interface Customer {
    id:string,
    name: string,
    years: number,
    income:number 
    category?: string,
    ST?: number
       
}
