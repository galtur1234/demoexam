import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  email:string;
  password:string;
  errorMessage:string; 
  isError:boolean = false; 


  onSubmit(){
    this.auth.SignUp(this.email,this.password).then(
      res =>{
        console.log('Succesful login;');
        this.router.navigate(['/customers']); 
      }
    ).catch(
      err => {
        console.log(err);
        this.isError = true; 
        this.errorMessage = err.message; 

      } 
    )
  }


  constructor(private auth:AuthService, private router:Router) { }  ngOnInit(): void {
  }

}
