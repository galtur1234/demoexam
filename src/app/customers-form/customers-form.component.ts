import { Component, OnInit, Output, Input, EventEmitter} from '@angular/core';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customersform',
  templateUrl: './customers-form.component.html',
  styleUrls: ['./customers-form.component.css']
})
export class CustomersFormComponent implements OnInit {

  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();
  @Input() name:string;
  @Input()  years:number;
  @Input() income:number;
  @Input() id:string;
  @Input() formType:string;
  isError:boolean = false;

  updateParent(){
    let customer:Customer = {id:this.id, name:this.name, years:this.years, income: this.income};
    if(this.years>24 || this.years<0){
      this.isError = true;
    }else{
    this.update.emit(customer); 
    if(this.formType == "Add Customer"){
      this.name  = null;
      this.years = null; 
      this.income = null; 

    }
  }
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }


  onSubmit(){
    
  }


  constructor() { }

  ngOnInit(): void {
  }

}
