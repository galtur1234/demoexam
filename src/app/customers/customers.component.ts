import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';
import { PredictionService } from './../prediction.service';
@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
 // customers;
 customers$;
 userId:string;
 editstate = [];
panelOpenStat = false;
addCustomerFormOpen = false;

  constructor(private customersService:CustomersService , public authService:AuthService, private predictionService:PredictionService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.customers$ = this.customersService.getCustomers(this.userId);
      }
    )
  }

  deleteCustomer(id:string){
    this.customersService.deleteCustomer(this.userId,id); 
  }


  update(customer:Customer){
    this.customersService.updateCustomer(this.userId ,customer.id ,customer.name, customer.years, customer.income);
  }


  save(customer:Customer){
    this.customersService.save(this.userId ,customer.id ,customer.name, customer.years, customer.income ,customer.category,customer.ST=2);
  }

  add(customer:Customer){
    this.customersService.addCustomer(this.userId,customer.name,customer.years,customer.income,customer.category="",customer.ST=0); 
  }


  public predict(customer:Customer){
    this.predictionService.predict(customer.years,customer.income).subscribe(
      res => {
        console.log(res);
        if(res > 0.5){
          console.log('yes');
          customer.category='YES';
          customer.ST=1;
        } else {
          console.log('no');
          customer.category='NO';
          customer.ST=1;
        }
      }
    )
  }






}
