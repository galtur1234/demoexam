// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB2CmaQuis0tsy6guJovt944sdUD4YxYtU",
    authDomain: "demoexam-902b2.firebaseapp.com",
    projectId: "demoexam-902b2",
    storageBucket: "demoexam-902b2.appspot.com",
    messagingSenderId: "133544656677",
    appId: "1:133544656677:web:115e206f730d3a67efa4cc"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
